package handler

import (
	"crunchyroll-api/config"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

func Category(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	category := vars["category"]
	url := fmt.Sprintf("https://api.crunchyroll.com/list_series.0.json?session_id=%s&media_type=anime&fields=series.series_id,series.in_queue,series.name,series.media_count,series.portrait_image&limit=54&offset=0&filter=tag:%s&locale=enUS&version=1.1.21.0&connectivity_type=ethernet",
		config.Config.SessionID, category)
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

func Series(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	seriesID := vars["series_id"]

	url := fmt.Sprintf("https://api.crunchyroll.com/info.0.json?session_id=%s&series_id=%s&fields=series.series_id,series.name,series.description,series.portrait_image,series.landscape_image,series.in_queue,series.media_count,series.rating,series.genres,series.year&locale=enUS&version=1.1.21.0&connectivity_type=ethernet",
		config.Config.SessionID, seriesID)
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

func Episode(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	episodeID := vars["episode_id"]

	url := fmt.Sprintf("https://api.crunchyroll.com/info.0.json?session_id=%s&media_id=%s&fields=media.media_id,media.available,media.available_time,media.collection_id,media.collection_name,media.series_id,media.type,media.episode_number,media.name,media.description,media.screenshot_image,media.created,media.duration,media.playhead,media.premium_only,media.stream_data&locale=enUS&version=1.1.21.0&connectivity_type=ethernet",
		config.Config.SessionID, episodeID)
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}
