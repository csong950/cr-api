package main

import (
	"crunchyroll-api/config"
	"crunchyroll-api/handler"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

type Session struct {
	Data struct {
		SessionID string `json:"session_id"`
	} `json:"data"`
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func session(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if config.Config.SessionID == "" {
			//url := fmt.Sprintf("https://api.crunchyroll.com/start_session.0.json?access_token=LNDJgOit5yaRIWN&device_type=com.crunchyroll.windows.desktop&device_id=1a51d06d-NANI-4adb-b842-166b4524e531&locale=enUS&version=1.1.21.0&connectivity_type=ethernet")
			resp, err := http.Get("https://api.crunchyroll.com/start_session.0.json?access_token=LNDJgOit5yaRIWN&device_type=com.crunchyroll.windows.desktop&device_id=1a51d06d-NANI-4adb-b842-166b4524e531&locale=enUS&version=1.1.21.0&connectivity_type=ethernet")
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			defer resp.Body.Close()
			body, err := ioutil.ReadAll(resp.Body)

			var s Session
			err = json.Unmarshal(body, &s)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			fmt.Println(s.Data.SessionID)
			config.Config.SessionID = s.Data.SessionID
		}
		next.ServeHTTP(w, r)
	})
}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`{"title": "chruncyroll api"}`))
	})
	r.HandleFunc("/category/{category}", handler.Category)
	r.HandleFunc("/series/{series_id}", handler.Series)
	r.HandleFunc("/series/{series_id}/{episode_id}", handler.Episode)

	r.Use(session)
	http.Handle("/", r)
	http.ListenAndServe(":8080", nil)

}
