## Endpoints
### Category
 http://localhost:8080/category/{category} category value: action, adventure, comedy, drama, fantasy, harem, historical, idols, isekai magical girls, mecha music .... 

Example: http://localhost:8080/category/action

### Series
  http://localhost:8080/series/{series_id} series_is is numeric series id. 

Example: http://localhost:8080/series/272781

### Episode
http://localhost:8080/series/{series_id}/{episode_id} also episode id is numeric value. 

Example http://localhost:8080/series/272781/796622# cr-api
